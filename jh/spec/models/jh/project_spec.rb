# frozen_string_literal: true

require 'spec_helper'

RSpec.describe Project do
  describe '#after_import' do
    context 'content validation' do
      let(:user) { create(:user) }
      let(:wiki) { project.wiki }
      let(:wiki_page) { create(:wiki_page, wiki: wiki) }
      let(:import_state) { create(:import_state, project: project) }

      before do
        allow(Gitlab::CurrentSettings).to receive(:content_validation_endpoint_enabled?).and_return(true)
        allow(Gitlab).to receive(:dev_env_or_com?).and_return(true)
        allow(import_state).to receive(:finish)
        project
        wiki_page
      end

      context "with public project" do
        let(:project) { create(:project, :repository, :wiki_repo, :public, creator: user) }

        it 'call ContentValidation::ContainerService for project and wiki valdiation' do
          # rubocop: disable CodeReuse/ActiveRecord
          expect(ContentValidation::ContainerService).to receive(:new)
            .with(hash_including(container: project, user: user)).ordered.and_call_original
          expect(ContentValidation::ContainerService).to receive(:new)
            .with(hash_including(container: wiki, user: user)).ordered.and_call_original
          # rubocop: enable CodeReuse/ActiveRecord
          project.after_import
        end
      end

      context "with private project" do
        let(:project) { create(:project, :repository, :wiki_repo, :private, creator: user) }

        it 'not validation when project is private' do
          expect(ContentValidation::ContainerService).not_to receive(:new)
          project.after_import
        end
      end
    end
  end
end
