import path from 'path';
import SKIP_LIST from '../skip_list.json';

const ROOT_PATH = path.resolve(__dirname, '../../../../');

const specEnv = global.jasmine.getEnv();

specEnv.specFilter = (spec) => {
  const currentSpecName = spec.getFullName();
  const currentTestFile = spec.result.testPath;

  const testPath = path.relative(ROOT_PATH, currentTestFile);
  const skipTestData = SKIP_LIST.by_case[testPath];

  if (typeof skipTestData !== 'undefined') {
    return !skipTestData.includes(currentSpecName);
  }
  return true;
};
