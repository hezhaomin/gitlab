# frozen_string_literal: true

RSpec.configure do |config|
  config.before(:suite) do
    FactoryBot.definition_file_paths = [
      Rails.root.join('jh', 'spec', 'factories')
    ]
    FactoryBot.find_definitions
  end
end
