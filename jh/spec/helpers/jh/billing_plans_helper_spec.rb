# frozen_string_literal: true

require 'spec_helper'

RSpec.describe BillingPlansHelper do
  describe '#number_to_plan_currency' do
    it 'returns the correct value and unit' do
      expect(helper.number_to_plan_currency(3.14)).to eq '¥3.14'
    end
  end

  describe '#use_new_purchase_flow?' do
    it 'returns false in JiHu' do
      expect(helper.use_new_purchase_flow?(1)).to be_falsey
    end
  end
end
