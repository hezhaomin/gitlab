---
info: For assistance with this Style Guide page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments-to-other-projects-and-subjects.
description: 'Writing styles, markup, formatting, and other standards for GitLab Documentation.'
---

# 翻译前必读

开始翻译前，请**务必**阅读以下内容，在翻译过程中如果遇到类似情况，请按照文中说明处理。

**编写规范**

您可以阅读[编写规范](http://about1s.gitlab.cn/handbook/about/style-guide/)，了解 Markdown 标准格式。

特别需要注意空格的使用，即英文和中文之间、数字和中文之间应使用空格，使版面更加整齐。

**Header**

标题上方的 Header 不需要翻译，注意不要更改任何格式。

例如：

```
---
info: For assistance with this Style Guide page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments-to-other-projects-and-subjects.
description: 'Writing styles, markup, formatting, and other standards for GitLab Documentation.'
---
```

**特殊符号**

由于文档工具中约定了部分标记格式，形如 `**{admin}**`，在线上会被转成统一图标，因此遇到这种情况，不需要翻译其中的文字。

示例：

```
1. On the top bar, select **Menu >** **{admin}** **Admin**.
```

应翻译为：

```
1. 在顶部导航栏中，选择 **目录 >** **{admin}** **管理员**。
```

**标题标记**

文档的标题可能会包含版本等级标记，此部分保持原地不动即可。

示例：

```
### Zero downtime updates **(PREMIUM SELF)**
```

应翻译为：

```
### 零宕机升级 **(PREMIUM SELF)**
```

**普通提示框**

使用 `>` 会生成普通提示框，保持原地不动即可。

示例：

```
> - Level of complexity: **Medium**
```

应翻译为：

```
> - 复杂级别：**中**
```

**警告提示框**

遇到以 `NOTE:`、`WARNING:` 开头，然后另起一行书写内容的情况时，在线上会被转为提示框，翻译所书写的内容即可。

示例：

```
NOTE:
The word note here should not be translated.
```

应翻译为：

```
NOTE:
这里的 note 不需要翻译。
```

除此之外的相似写法需要翻译。

示例：

```
**Note**：The word note here should be translated.
```

应翻译为：

```
**注意**：这里的 note 需要翻译。
```

**本文内链接**

文档内的部分链接直接使用 `(#heading)`，可以定位跳转到文内的其它标题。由于标题内容翻译成中文，所以这里的 `heading` 需要和翻译后的标题内容保持一致，其中如果有空格需要使用 `-` 代替。

示例：

```
See [chart settings](#chart-settings).
```

应翻译为：

```
查看 [chart 设置](#chart-设置)。
```

**本文外链接**

包含以下两种情况，需要分别处理：

* 使用相对目录路径：`[external Gitaly documentation](../../../advanced/external-gitaly/)`

	应翻译为：`[外部 Gitaly 文档](../../../advanced/external-gitaly/)`
	
	其中相对目录路径不需要处理。
	
* 使用域名：`[GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent/)`

	应翻译为：`[GitLab Kubernetes 代理](https://docs.gitlab.cn/jh/user/clusters/agent/)`
	
	由于英文原文使用的域名为 global 方面的文档中心，需要 translator 帮忙替换域名的前半部分：
	
	* `https://docs.gitlab.com/ee` 替换为 `https://docs.gitlab.cn/jh`
	* `https://docs.gitlab.com/omnibus` 替换为 `https://docs.gitlab.cn/omnibus`
	* `https://docs.gitlab.com/runner` 替换为 `https://docs.gitlab.cn/runner`
	* `https://docs.gitlab.com/charts` 替换为 `https://docs.gitlab.cn/charts`

**外部链接**

包含以下三种情况，需要分别处理：

* 极狐GitLab SaaS 服务和相关网站：`[sign up page](https://gitlab.com/users/sign_up)`

	应翻译为：`[注册页面](https://gitlab.cn/users/sign_up)`
	
	再比如 `https://about.gitlab.com/install`，可以替换为 `https://about.gitlab.cn/install`。替换域名后，请检查下是否可以正常访问，如果不可以，先保持原地不动。
	
* GitLab 代码库链接：`[NGINX chart documentation](https://gitlab.com/gitlab-org/charts/gitlab/blob/master/charts/nginx-ingress/index.md#configuration)`
	
	这种情况一般是引用存放在原文档库中的脚本示例，需要将 Global 的代码库地址替换为我们的代码库地址，如下所述。
	
	* `https://gitlab.com/gitlab-org/gitlab` 替换为 `https://gitlab.com/gitlab-jh/gitlab`
	* `https://gitlab.com/gitlab-org/omnibus-gitlab` 替换为 `https://gitlab.cn/gitlab-cn/omnibus-gitlab`
	* `https://gitlab.com/gitlab-org/gitlab-runner` 替换为 `https://gitlab.cn/gitlab-cn/gitlab-runner`
	* `https://gitlab.com/gitlab-org/charts/gitlab` 替换为 `https://gitlab.cn/gitlab-cn/charts/gitlab`

	因此，示例应翻译为：`[NGINX chart 文档](https://gitlab.cn/gitlab-cn/charts/gitlab/blob/master/charts/nginx-ingress/index.md#configuration)`

	
* 外部网站链接：`[Container Registry](https://gcr.io/google_containers)`

	应翻译为：`[容器镜像库](https://gcr.io/google_containers)`
	
	保持原地不动即可。

**注释**

由于各种原因，比如 translator 觉得某段内容不确定是否应放在中文文档中，或者不知道应如何翻译等等，您可以在翻译后的文档中保留英文原文，并将其注释掉，暂时不处理。

示例：

```
The `hostSuffix` is appended to the subdomain when assembling a hostname using the
base `domain`, but is not used for hosts that have their own `name` set.
```

注释处理为：

```
<!--
The `hostSuffix` is appended to the subdomain when assembling a hostname using the
base `domain`, but is not used for hosts that have their own `name` set.
-->
```

**图片和视频**

如果遇到引用图片或视频的内容，translator 保持原地不动即可。

示例：

```
![Reference Architectures](img/reference-architectures.png)
```
