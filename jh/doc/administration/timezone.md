---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 更改您的时区 **(FREE SELF)**

全局时区配置参数可以在 `config/gitlab.yml` 中更改：

```plaintext
# time_zone: 'UTC'
```

如果要更改 GitLab 应用程序的默认时区，请取消注释并自定义。

## 查看可用时区

要查看所有可用的时区，请运行 `bundle exec rake time:zones:all`。

对于 Omnibus 安装实例，运行 `gitlab-rake time:zones:all`。

NOTE:
此 Rake 任务不会在重新配置期间以 Omnibus GitLab 所需的 TZInfo 格式列出时区

## 在 Omnibus 安装实例中更改时区

GitLab 将其时区默认为 UTC。在`/etc/gitlab/gitlab.rb` 中有一个全局时区配置参数。

要获取时区列表，请登录到您的 GitLab 应用程序服务器并运行一个命令，该命令为服务器生成 TZInfo 格式的时区列表。例如，安装 `timedatectl` 并运行 `timedatectl list-timezones`。

要更新，请添加最适合您所在位置的时区。 例如：

```ruby
gitlab_rails['time_zone'] = 'America/New_York'
```

添加配置参数后，重新配置并重启您的 GitLab 实例：

```shell
gitlab-ctl reconfigure
gitlab-ctl restart
```

<!--
## 更改每个用户的时区

> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/57654) in GitLab 11.11, disabled by default behind `user_time_settings` [feature flag](feature_flags.md).
> - [Enabled by default](https://gitlab.com/gitlab-org/gitlab/-/issues/29669) in GitLab 13.9.
> - [Feature flag removed](https://gitlab.com/gitlab-org/gitlab/-/issues/29669) in GitLab 14.1.

A user can set their time zone in their profile. If a user has not set their time zone, it defaults
to the time zone [configured at the instance level](#changing-your-time-zone). On GitLab.com, the
default time zone is UTC.

For more information, see [Set your time zone](../user/profile/index.md#set-your-time-zone).
-->