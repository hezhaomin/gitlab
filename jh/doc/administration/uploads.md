---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 上传文件管理 **(FREE SELF)**

上传文件是可能作为单个文件发送到极狐GitLab 的所有用户数据。例如，头像和笔记的附件是上传文件。上传文件是极狐GitLab 功能不可或缺的一部分，因此无法禁用。

## 使用本地存储

这是默认配置。要更改本地存储上传的位置，请根据您的安装方法使用本节中的步骤：

NOTE:
由于历史原因，实例级别的上传文件（例如 favicon<!--[favicon](../user/admin_area/appearance.md#favicon)-->）存储在基本目录中，默认情况下为 `uploads/-/system`。强烈建议不要更改现有极狐GitLab 安装的基本目录。

**在 Omnibus GitLab 安装实例：**

*上传的文件默认存储在 `/var/opt/gitlab/gitlab-rails/uploads` 中。*

1. 例如，要将存储路径更改为 `/mnt/storage/uploads`，请编辑 `/etc/gitlab/gitlab.rb` 并添加以下行：

   ```ruby
   gitlab_rails['uploads_directory'] = "/mnt/storage/uploads"
   ```

   此设置仅适用于您尚未更改 `gitlab_rails['uploads_storage_path']` 目录的情况。

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。

**在源安装：**

*上传的内容默认存储在 `/home/git/gitlab/public/uploads`。*

1. 要将存储路径更改为例如 `/mnt/storage/uploads`，请编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下行：

   ```yaml
   uploads:
     storage_path: /mnt/storage
     base_dir: uploads
   ```

1. 保存文件并[重新启动极狐GitLab](restart_gitlab.md#源安装实例) 以使更改生效。

## 使用对象存储 **(FREE SELF)**

如果您不想使用安装了极狐GitLab 的本地磁盘来存储上传，您可以改用像 AWS S3 这样的对象存储提供商。此配置依赖于已配置的有效 AWS 凭证。

<!--
[阅读有关在 GitLab 中使用对象存储的更多信息](object_storage.md)。

我们建议使用[整合对象存储设置](object_storage.md#整合的对象存储配置)。以下说明适用于原始配置格式。
-->

### 对象存储设置

对于源安装，以下设置嵌套在 `uploads:` 和 `object_store:` 下。在 Omnibus GitLab 安装中，它们以 `uploads_object_store_` 为前缀。

| 设置 | 描述 | 默认值 |
|---------|-------------|---------|
| `enabled` | 启用/禁用对象存储 | `false` |
| `remote_directory` | 将存储上传的存储桶名称 | |
| `direct_upload` | 设置为 `true` 以从上传路径中删除 Puma。Workhorse 处理实际的 Artifact 上传到 Object Storage，而 Puma 进行最少的处理来跟踪上传。不需要本地共享存储。如果引入了对所有文件的单一存储类型的支持，则可能会删除该选项。<!--阅读有关[直接上传](../development/uploads.md#direct-upload) 的更多信息。--> | `false` |
| `background_upload` | 设置为 `false` 以禁用自动上传。 一旦上传直接到 S3，选项可能会被删除（如果 `direct_upload` 设置为 `true`，它将覆盖 `background_upload`） | `true` |
| `proxy_download` | 设置为 `true` 以启用代理服务的所有文件。选项允许减少出口流量，因为这允许客户端直接从远程存储下载而不是代理所有数据 | `false` |
| `connection` | 下面描述的各种连接选项 | |

#### 连接设置

<!--请参阅[不同提供商的可用连接设置](object_storage.md#连接设置)。-->

**在 Omnibus 安装实例：**

*上传文件默认存储在 `/var/opt/gitlab/gitlab-rails/uploads` 中。*

1. 编辑 `/etc/gitlab/gitlab.rb` 并通过添加以下行替换为您想要的值：

   ```ruby
   gitlab_rails['uploads_object_store_enabled'] = true
   gitlab_rails['uploads_object_store_remote_directory'] = "uploads"
   gitlab_rails['uploads_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'aws_access_key_id' => 'AWS_ACCESS_KEY_ID',
     'aws_secret_access_key' => 'AWS_SECRET_ACCESS_KEY'
   }
   ```

   如果您使用 AWS IAM 配置文件，请确保省略 AWS 访问密钥和 secret 访问密钥/值对。

   ```ruby
   gitlab_rails['uploads_object_store_connection'] = {
     'provider' => 'AWS',
     'region' => 'eu-central-1',
     'use_iam_profile' => true
   }
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。
1. 使用 [`gitlab:uploads:migrate:all` Rake 任务](raketasks/uploads/migrate.md) 将任何现有的本地上传迁移到对象存储。

**在源安装实例：**

*上传的内容默认存储在 `/home/git/gitlab/public/uploads`。*

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下几行：

   ```yaml
   uploads:
     object_store:
       enabled: true
       remote_directory: "uploads" # The bucket name
       connection:
         provider: AWS # Only AWS supported at the moment
         aws_access_key_id: AWS_ACCESS_KEY_ID
         aws_secret_access_key: AWS_SECRET_ACCESS_KEY
         region: eu-central-1
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。
1. 使用 [`gitlab:uploads:migrate:all` Rake 任务](raketasks/uploads/migrate.md) 将任何现有的本地上传迁移到对象存储。

#### OpenStack 示例

**在 Omnibus 安装实例：**

_上传文件默认存储在 `/var/opt/gitlab/gitlab-rails/uploads` 中。_

1. 编辑 `/etc/gitlab/gitlab.rb` 并通过添加以下行替换为您想要的值：

   ```ruby
   gitlab_rails['uploads_object_store_remote_directory'] = "OPENSTACK_OBJECT_CONTAINER_NAME"
   gitlab_rails['uploads_object_store_connection'] = {
    'provider' => 'OpenStack',
    'openstack_username' => 'OPENSTACK_USERNAME',
    'openstack_api_key' => 'OPENSTACK_PASSWORD',
    'openstack_temp_url_key' => 'OPENSTACK_TEMP_URL_KEY',
    'openstack_auth_url' => 'https://auth.cloud.ovh.net/v2.0/',
    'openstack_region' => 'DE1',
    'openstack_tenant' => 'TENANT_ID',
   }
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。
1. 使用 [`gitlab:uploads:migrate:all` Rake 任务](raketasks/uploads/migrate.md) 将任何现有的本地上传迁移到对象存储。

---

**在源安装实例：**

*上传的内容默认存储在 `/home/git/gitlab/public/uploads` 中。*

1. 编辑 `/home/git/gitlab/config/gitlab.yml` 并添加或修改以下几行：

   ```yaml
   uploads:
     object_store:
       enabled: true
       direct_upload: false
       background_upload: true
       proxy_download: false
       remote_directory: OPENSTACK_OBJECT_CONTAINER_NAME
       connection:
         provider: OpenStack
         openstack_username: OPENSTACK_USERNAME
         openstack_api_key: OPENSTACK_PASSWORD
         openstack_temp_url_key: OPENSTACK_TEMP_URL_KEY
         openstack_auth_url: 'https://auth.cloud.ovh.net/v2.0/'
         openstack_region: DE1
         openstack_tenant: 'TENANT_ID'
   ```

1. 保存文件并[重新配置极狐GitLab](restart_gitlab.md#omnibus-gitlab-reconfigure) 以使更改生效。
1. 使用 [`gitlab:uploads:migrate:all` Rake 任务](raketasks/uploads/migrate.md) 将任何现有的本地上传迁移到对象存储。
