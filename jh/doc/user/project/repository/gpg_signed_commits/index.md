---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: concepts, howto
---

# GPG 签名提交 **(FREE)**

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/9546) in GitLab 9.5.
> - Subkeys support was added in GitLab 10.1.
-->

您可以使用 GPG 密钥对在极狐GitLab 仓库中进行的 Git 提交进行签名。如果可以验证提交者的身份，则签名提交将标记为 **已验证**。为了验证提交者的身份，极狐GitLab 需要他们的公共 GPG 密钥。

NOTE:
术语 GPG 用于所有 OpenPGP/PGP/GPG 相关材料和实现。

要查看用户的公共 GPG 密钥，您可以：

- 转到 `https://gitlab.example.com/<username>.gpg`。
- 选择用户个人资料右上角的 **查看公共 GPG 密钥** (**{key}**)。

尚不支持 GPG 验证标签。

<!--
See the [further reading](#further-reading) section for more details on GPG.
-->

## 如何处理 GPG

极狐GitLab 使用自己的密钥环来验证 GPG 签名。它不访问任何公钥服务器。

对于要由极狐GitLab 验证的提交：

- 提交者必须有一个 GPG 公钥/私钥对。
- 提交者的公钥必须已上传到他们的极狐GitLab 帐户。
- GPG 密钥中的电子邮件之一必须与极狐GitLab 中提交者使用的**已验证**电子邮件地址匹配。该地址将成为公钥的一部分。如果您想将此地址保密，请使用极狐GitLab 在您的个人资料中提供的自动生成的[私人提交电子邮件地址](../../../profile/index.md#使用自动生成的私人提交电子邮件)。
- 提交者的电子邮件地址必须与 GPG 密钥中经过验证的电子邮件地址相匹配。

## 生成 GPG 密钥

如果您还没有 GPG 密钥，以下步骤可以帮助您入门：

1. [安装 GPG](https://www.gnupg.org/download/index.html)，适用于您的操作系统。如果您的操作系统安装了 `gpg2`，请在以下命令中将 `gpg` 替换为 `gpg2`。
1. 使用适合您的 `gpg` 版本的命令生成私钥/公钥对。这个命令会产生一系列问题：

   ```shell
   # Use this command for the default version of gpg, including
   # Gpg4win on Windows, and most macOS versions:
   gpg --gen-key

   # Use this command for versions of GPG later than 2.1.17:
   gpg --full-gen-key
   ```

1. 第一个问题是可以使用哪种算法。选择您想要的种类或按 <kbd>Enter</kbd> 选择默认值（RSA 和 RSA）：

   ```plaintext
   Please select what kind of key you want:
      (1) RSA and RSA (default)
      (2) DSA and Elgamal
      (3) DSA (sign only)
      (4) RSA (sign only)
   Your selection? 1
   ```

1. 下一个问题是密钥长度。我们建议您选择`4096`：

   ```plaintext
   RSA keys may be between 1024 and 4096 bits long.
   What keysize do you want? (2048) 4096
   Requested keysize is 4096 bits
   ```

1. 指定您的密钥的有效期。 这是主观的，您可以使用默认值，即永不过期：

   ```plaintext
   Please specify how long the key should be valid.
            0 = key does not expire
         <n>  = key expires in n days
         <n>w = key expires in n weeks
         <n>m = key expires in n months
         <n>y = key expires in n years
   Key is valid for? (0) 0
   Key does not expire at all
   ```

1. 输入`y`确认您给出的答案是正确的：

   ```plaintext
   Is this correct? (y/N) y
   ```

1. 输入您的真实姓名、与此密钥关联的电子邮件地址（应与您在极狐GitLab 中使用的已验证电子邮件地址匹配）和可选评论（按 <kbd>Enter</kbd> 跳过）：

   ```plaintext
   GnuPG needs to construct a user ID to identify your key.

   Real name: Mr. Robot
   Email address: <your_email>
   Comment:
   You selected this USER-ID:
       "Mr. Robot <your_email>"

   Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? O
   ```

1. 询问时选择一个强密码并输入两次以确认。
1. 使用以下命令列出您刚刚创建的私有 GPG 密钥：

   ```shell
   gpg --list-secret-keys --keyid-format LONG <your_email>
   ```

   将 `<your_email>` 替换为您在上面输入的电子邮件地址。

1. 复制以 `sec` 开头的 GPG 密钥 ID。 在以下示例中是 `30F2B65B9246B6CA`：

   ```plaintext
   sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]
         D5E4F29F3275DC0CDA8FFC8730F2B65B9246B6CA
   uid                   [ultimate] Mr. Robot <your_email>
   ssb   rsa4096/B7ABC0813E4028C0 2017-08-18 [E]
   ```

1. 导出该 ID 的公钥（替换上一步中的密钥 ID）：

   ```shell
   gpg --armor --export 30F2B65B9246B6CA
   ```

1. 最后，复制公钥并[在您的用户设置中添加](#将-gpg-密钥添加到您的帐户)

## 添加 GPG 密钥到您的帐户

NOTE:
添加密钥后，您将无法对其进行编辑，只能将其删除。如果粘贴不起作用，您必须删除有问题的密钥并重新添加它。

您可以在用户设置中添加 GPG 密钥：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **GPG 密钥**。
1. 将您的*公*钥粘贴到 **密钥** 文本框中。

   ![Paste GPG public key](img/profile_settings_gpg_keys_paste_pub.png)

1. 选择 **添加密钥** 将其添加到极狐GitLab。您可以看到密钥的指纹、相应的电子邮件地址和创建日期。

   ![GPG key single page](img/profile_settings_gpg_keys_single_key.png)

## 将您的 GPG 密钥与 Git 相关联

在您[创建您的 GPG 密钥](#生成-gpg-密钥)并[将其添加到您的帐户](#添加-gpg-密钥到您的帐户)之后，是时候告诉 Git 使用哪个密钥了。

1. 使用以下命令列出您刚刚创建的私有 GPG 密钥：

   ```shell
   gpg --list-secret-keys --keyid-format LONG <your_email>
   ```

   将 `<your_email>` 替换为您在上面输入的电子邮件地址。

1. 复制以 `sec` 开头的 GPG 密钥 ID。在下面的例子中，它是 `30F2B65B9246B6CA`：

   ```plaintext
   sec   rsa4096/30F2B65B9246B6CA 2017-08-18 [SC]
         D5E4F29F3275DC0CDA8FFC8730F2B65B9246B6CA
   uid                   [ultimate] Mr. Robot <your_email>
   ssb   rsa4096/B7ABC0813E4028C0 2017-08-18 [E]
   ```

1. 告诉 Git 使用该密钥对提交进行签名：

   ```shell
   git config --global user.signingkey 30F2B65B9246B6CA
   ```

   将 `30F2B65B9246B6CA` 替换为您的 GPG 密钥 ID。

1. （可选）如果 Git 正在使用 `gpg` 并且您得到类似 `secret key not available` 或 `gpg: signature failed: secret key not available` 的错误，运行以下命令以更改为 `gpg2`：

   ```shell
   git config --global gpg.program gpg2
   ```

## 签名提交

在您[创建您的 GPG 密钥](#生成-gpg-密钥)并[将其添加到您的帐户](#添加-gpg-密钥到您的帐户) 后，您可以开始签名您的提交：

1. 像以前一样提交，唯一的区别是添加了 `-S` 标志：

   ```shell
   git commit -S -m "My commit msg"
   ```

1. 询问时输入 GPG 密钥的密码。
1. 推送到极狐GitLab 并检查您的提交[已验证](#验证提交)。

如果你不想在每次提交时都输入 `-S` 标志，您可以告诉 Git 自动签名您的提交：

```shell
git config --global commit.gpgsign true
```

## 验证提交

1. 在项目或 [合并请求](../../merge_requests/index.md) 中，导航到 **提交** 选项卡。签名提交显示一个包含 **已验证** 或 **未验证** 的徽章，具体取决于 GPG 签名的验证状态。

   ![Signed and unsigned commits](img/project_signed_and_unsigned_commits.png)

1. 通过单击 GPG 徽章，将显示签名的详细信息。

   ![Signed commit with verified signature](img/project_signed_commit_verified_signature.png)

   ![Signed commit with verified signature](img/project_signed_commit_unverified_signature.png)

## 撤销 GPG 密钥

撤销密钥**取消验证**已经签名的提交。使用此密钥验证的提交更改为未验证状态。撤销此密钥后，未来的提交将保持未验证。如果您的密钥已被泄露，则应使用此操作。

要撤销 GPG 密钥：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **GPG 密钥**。
1. 选择要删除的 GPG 密钥旁边的**撤销**。

## 删除 GPG 密钥

删除密钥**不会取消验证**已经签名的提交。使用此密钥验证的提交保持验证状态。删除此密钥后，只有未推送的提交保持未验证状态。 要取消验证已签名的提交，您需要从您的帐户中[撤销关联的 GPG 密钥](#撤销-gpg-密钥)。

要从您的帐户中删除 GPG 密钥：

1. 在右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **GPG 密钥**。
1. 选择要删除的 GPG 密钥旁边的垃圾桶图标 (**{remove}**)。

## 拒绝未签名的提交 **(PREMIUM)**

您可以将您的项目配置为，通过推送规则<!--[推送规则](../../../../push_rules/push_rules.md)-->拒绝非 GPG 签名的提交。

<!--
## GPG signing API

Learn how to [get the GPG signature from a commit via API](../../../../api/commits.md#get-gpg-signature-of-a-commit).

## Further reading

For more details about GPG, see:

- [Git Tools - Signing Your Work](https://git-scm.com/book/en/v2/Git-Tools-Signing-Your-Work)
- [Managing OpenPGP Keys](https://riseup.net/en/security/message-security/openpgp/gpg-keys)
- [OpenPGP Best Practices](https://riseup.net/en/security/message-security/openpgp/best-practices)
- [Creating a new GPG key with subkeys](https://www.void.gr/kargig/blog/2013/12/02/creating-a-new-gpg-key-with-subkeys/) (advanced)
- [Review existing GPG keys in your instance](../../../admin_area/credentials_inventory.md#review-existing-gpg-keys)
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
