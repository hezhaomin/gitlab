---
stage: Create
group: Source Code
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference
---

# Git 属性 **(FREE)**

GitLab 支持定义自定义 [Git 属性](https://git-scm.com/docs/gitattributes)，例如将哪些文件视为二进制文件，以及用于语法突出显示差异的语言。

要定义这些属性，请在存储库的根目录中创建一个名为 `.gitattributes` 的文件，并将其推送到项目的默认分支。

## 编码要求

`.gitattributes` 文件*必须*以 UTF-8 编码并且*不得*包含字节顺序标记。如果使用不同的编码，文件的内容将被忽略。

## 语法高亮

`.gitattributes` 文件可用于定义在语法高亮文件和差异时使用的语言。<!--有关更多信息，请参阅 ["语法高亮"](highlighting.md)。-->
