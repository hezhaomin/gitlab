---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 合并请求版本

每次推送到与合并请求相关的分支时，都会创建一个新版本的合并请求差异。当您访问包含多个推送的合并请求时，您可以选择并比较这些合并请求差异的版本。

![Merge request versions](img/versions.png)

## 选择版本

默认情况下，显示最新版本的更改。但是，您可以从版本下拉列表中选择较旧的版本。

![Merge request versions dropdown](img/versions_dropdown.png)

合并请求版本基于推送而非提交。因此，如果您一次推送 5 次提交，它会在下拉列表中显示为单个选项。如果您按了 5 次，则计为 5 个选项。

您还可以将合并请求版本与旧版本进行比较，以查看自那时以来发生了什么变化。

![Merge request versions compare](img/versions_compare.png)

查看过时的合并版本或与 base 以外的版本进行比较时，评论被禁用。

每次将新更改推送到分支时，都会出现一个比较上次更改的链接作为系统注释。

![Merge request versions system note](img/versions_system_note.png)

## 查找引入更改的合并请求

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/2383) in GitLab 10.5.
-->

在查看提交详细信息页面时，极狐GitLab 链接到包含该提交的合并请求（或多个合并请求）。

这仅适用于最新版本的合并请求中的提交 - 如果提交位于合并请求中，然后从该合并请求中变基，则它们不会被链接。

## 合并请求的`HEAD`比较模式

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/27008) in GitLab 12.10.
-->

合并请求，尤其是 **变更** 选项卡，是审核和讨论源代码的地方。在目标分支被合并到合并请求的源分支的情况下，源分支和目标分支中的更改可以混合在一起显示，这使得很难理解正在添加哪些更改以及目标分支中已经存在哪些更改。

在 12.10 版本中，我们添加了一个比较模式，该模式显示了通过模拟合并来计算的差异 - 更准确地表示更改而不是使用两个分支的 base。 通过选择 **main (HEAD)**，可以从比较目标下拉菜单中使用新模式。在 13.9 版本中，它取代了旧的默认比较。<!--For technical details, additional information is available in the
[developer documentation](../../../development/diffs.md#merge-request-diffs-against-the-head-of-the-target-branch).-->

![Merge request versions compare HEAD](img/versions_compare_head_v12_10.png)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
