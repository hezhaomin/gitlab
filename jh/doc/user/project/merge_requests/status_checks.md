---
stage: Manage
group: Compliance
info: "To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments"
type: reference, concepts
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/merge_requests/status_checks.html'
---

# 外部状态检查 **(ULTIMATE)**

> - 引入于 14.0 版本，在功能标志 `:ff_external_status_checks` 后默认禁用
> - 功能标志移除于 14.1 版本。

您可以创建一个状态检查，将合并请求数据发送到第三方工具。
当用户创建、更改或关闭合并请求时，极狐GitLab 会发送通知。然后，用户或自动化工作流可以更新来自极狐GitLab 外部的合并请求的状态。

通过这种集成，您可以与第三方工作流工具（如 ServiceNow）或您选择的自定义工具集成。第三方工具以关联状态进行响应。 然后，此状态在合并请求中显示为非阻塞部件，以将此状态显示给合并请求作者或合并请求本身级别的审核者。

缺少状态检查响应不会阻止合并请求的合并。

您可以为每个单独的项目配置合并请求状态检查，不在项目之间共享。

<!--
To learn more about use cases, feature discovery, and development timelines,
see the [external status checks epic](https://gitlab.com/groups/gitlab-org/-/epics/3869).
-->

## 在项目上查看状态检查

在每个项目的设置中，您可以看到添加到项目的状态检查列表：

1. 在您的项目中，转到 **设置 > 通用**。
1. 展开 **合并请求** 部分。
1. 向下滚动到 **状态检查** 子部分。

![Status checks list](img/status_checks_list_view_v14_0.png)

此列表显示服务名称、API URL 和目标分支。
它还提供了允许您创建、编辑或删除状态检查的操作。

## 添加或更新状态检查

### 添加状态检查

在 **状态检查** 子部分中，选择 **添加状态检查** 按钮。
然后会显示 **添加状态检查** 表单。

![Status checks create form](img/status_checks_create_form_v14_0.png)

填写表格并选择 **添加状态检查** 按钮会创建一个新的状态检查。

### 更新装填检查

在 **状态检查** 子部分中，选择要编辑的状态检查旁边的 **编辑** 按钮。
然后会显示 **更新状态检查** 表单。

![Status checks update form](img/status_checks_update_form_v14_0.png)

更改表单中的值并选择 **更新状态检查** 按钮更新状态检查。

### 表单值

对于常见的表单错误，请参阅下面的[故障排查](#故障排查)部分。

#### Service name

此名称可以是任何包含字母和数字的值，并且 **必须** 设置。 名称 **必须** 对项目来说是唯一的。
名称 **必须** 对项目而言是唯一的。

#### API to check

此字段需要一个 URL，并且 **必须** 使用 HTTP 或 HTTPS 协议。
我们 **建议** 使用 HTTPS 来保护传输中的合并请求数据。
URL **必须** 被设置并且**必须**对于项目来说是唯一的。

#### 目标分支

如果要将状态检查限制为单个分支，可以使用此字段设置此限制。

![Status checks branch selector](img/status_checks_branches_selector_v14_0.png)

分支列表是由项目<!--[受保护的分支](../protected_branches.md)-->受保护的分支填入的。

当有很多分支并且您要查找的分支没有立即出现时，您可以滚动分支列表或使用搜索框。搜索框需要输入 **三个** 包含字母和数字的字符才能开始搜索。

如果您希望状态检查应用于 **所有** 合并请求，您可以选择 **所有分支** 选项。

## 删除状态检查

在 **状态检查** 子部分中，选择要删除的状态检查旁边的 **删除...** 按钮。
然后会显示 **删除状态检查** 窗口。

![Status checks delete modal](img/status_checks_delete_modal_v14_0.png)

要完成状态检查的删除，您必须选择 **删除状态检查** 按钮。将 **永久** 删除状态检查，并且 **将无法** 恢复。

## 状态检查部件

> - 引入于 14.1 版本

状态检查部件显示在合并请求中，并显示外部状态检查的状态：

![Status checks widget](img/status_checks_widget_passed_v14_0.png)

如果外部状态检查未通过，组织可能具有不允许合并合并请求的策略。但是，部件中的详细信息仅供参考。系统不会阻止合并未通过状态检查的合并请求。

当极狐GitLab 等待来自外部状态检查的响应时，部件将状态检查显示为 `pending`：

![Status checks widget pending](img/status_checks_widget_pending_v14_0.png)

在极狐GitLab 收到响应<!--[收到响应](../../../api/status_checks.md#set-status-of-an-external-status-check)-->后，部件会相应地更新。

NOTE:
极狐GitLab 无法保证相关外部服务正确处理外部状态检查。

## 故障排查

### 重复值错误

```plaintext
Name is already taken
---
External API is already in use by another status check
```

在每个项目的基础上，状态检查只能使用一次名称或 API URL。
这些错误意味着此项目状态检查中已使用状态检查名称或 API URL。

您必须在当前状态检查中选择不同的值或更新现有状态检查中的值。

### 无效 URL 错误

```plaintext
Please provide a valid URL
```

要检查的 API 字段需要提供的 URL 以使用 HTTP 或 HTTPS 协议。
您必须更新该字段的值以满足此要求。

### 检索或搜索期间的分支列表错误

```plaintext
Unable to fetch branches list, please close the form and try again
```

从分支检索 API 收到意外响应。
按照建议，您应该关闭表单并重新打开或刷新页面。此错误应该是暂时的。<!--，但如果它仍然存在，请检查 [GitLab 状态页面](https://status.gitlab.com/) 以查看是否存在更广泛的中断。-->

### 无法加载状态检查

```plaintext
Failed to load status checks
```

从外部状态检查 API 收到意外响应。

您应该：

- 如果此错误是暂时的，请刷新页面。
- 如果问题仍然存在，请检查<!--[GitLab 状态页面](https://status.gitlab.com/)-->看看是否有更广泛的中断。

<!--
## Related links

- [External status checks API](../../../api/status_checks.md)
-->