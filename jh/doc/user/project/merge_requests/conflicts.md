---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, concepts
---

# 合并冲突 **(FREE)**

当合并请求中的两个分支（源和目标）各自有不同的更改时，就会发生*合并冲突*，并且您必须决定接受哪个更改。在合并请求中，Git 逐行比较文件的两个版本。在大多数情况下，极狐GitLab 可以将更改合并在一起。但是，如果两个分支都更改了相同的行，极狐GitLab 会阻止合并，您必须选择要保留的更改。

合并请求无法合并，除非您：

- 创建合并提交。
- 通过变基解决冲突。

![Merge request widget](img/merge_request_widget.png)

## 您可以在用户界面中解决的冲突

如果您的合并冲突满足以下所有条件，您可以在 GitLab 用户界面中解决合并冲突：

- 文件是文本文件，不是二进制文件。
- 文件采用 UTF-8 兼容编码。
- 该文件尚未包含冲突标记。
- 添加了冲突标记的文件小于 200 KB。
- 该文件存在于两个分支的相同路径下。

如果合并请求中的任何文件包含冲突，但不能满足所有这些条件，则必须手动解决冲突。

## 极狐GitLab 无法检测到的冲突

当两个分支将文件重命名为不同名称时，极狐GitLab 不会检测冲突。
例如，这些更改不会产生冲突：

1. 在分支`a`上，执行 `git mv example.txt example1.txt`
1. 在分支 `b` 上，执行 `git mv example1.txt example3.txt`。

当这些分支合并时，`example1.txt` 和 `example3.txt` 都存在。

## 解决冲突的方法

极狐GitLab 在用户界面中显示了[可供解决的冲突](#您可以在用户界面中解决的冲突)，也可以通过命令行在本地解决冲突：

- [交互模式](#在交互模式下解决冲突)：UI 方法最适合冲突，只需要您选择要保留的行的哪个版本，无需编辑。
- [内联编辑器](#在内联编辑器中解决冲突)：UI 方法最适合更复杂的冲突，需要您编辑行并手动将更改混合在一起。
- [命令行](#在命令行解决冲突)：提供对最复杂冲突的完全控制。

## 在交互模式下解决冲突

从极狐GitLab 用户界面解决不太复杂的冲突：

1. 转到您的合并请求。
1. 选择 **概览**，然后滚动到合并请求报告部分。
1. 找到合并冲突消息，然后选择 **解决冲突**。极狐GitLab 显示具有合并冲突的文件列表。冲突突出显示：

   ![Conflict section](img/conflict_section.png)

1. 对于每个冲突，选择 **使用我们的** 或 **使用他们的** 来标记您要保留的冲突行的版本。这个决定被称为“解决冲突”。
1. 输入 **提交消息**。
1. 选择 **提交到源分支**。

解决冲突使用您选择的文本版本，将合并请求的目标分支合并到源分支中。如果源分支是 `feature`，目标分支是 `main`，这些动作类似于在本地运行 `git checkout feature； git merge main`。

## 在内联编辑器中解决冲突

一些合并冲突更复杂，需要您手动修改行来解决它们的冲突。使用合并冲突解决编辑器，解决极狐GitLab 界面中的复杂冲突：

1. 转到您的合并请求。
1. 选择 **概览**，然后滚动到合并请求报告部分。
1. 找到合并冲突消息，然后选择 **解决冲突**。极狐GitLab 显示具有合并冲突的文件列表。
1. 选择 **内联编辑** 打开编辑器：
   ![Merge conflict editor](img/merge_conflict_editor.png)
1. 解决冲突后，输入 **提交消息**。
1. 选择 **提交到源分支**。

## 在命令行解决冲突

虽然大多数冲突可以通过 GitLab 用户界面解决，但有些冲突太复杂了。
复杂的冲突最好从命令行在本地修复，让您可以最大程度地控制每个更改：

1. 打开终端并检查您的功能分支。 例如，`my-feature-branch`：

   ```shell
   git checkout my-feature-branch
   ```

1. [变基分支](../../../topics/git/git_rebase.md#常规变基) 针对目标分支（这里是 `main`），所以 Git 会提示您冲突：

   ```shell
   git fetch
   git rebase origin/main
   ```

1. 在偏好的代码编辑器中打开冲突文件。
1. 找到冲突块：
    - 它以标记开头：`<<<<<<< HEAD`。
    - 接下来，它会显示您的更改。
    - 标记 `========` 表示更改的结束。
    - 接下来，它显示目标分支中的最新更改。
    - 标记 `>>>>>>>` 表示冲突结束。
1. 编辑文件：
    1. 选择要保留的版本（在 `======` 之前或之后）。
    1. 删除不想保留的版本。
    1. 删除冲突标记。
1. 保存文件。
1. 对每个包含冲突的文件重复该过程。
1. 在 Git 中暂存您的更改：

   ```shell
   git add .
   ```

1. 提交您的更改：

   ```shell
   git commit -m "Fix merge conflicts"
   ```

1. 继续变基：

   ```shell
   git rebase --continue
   ```

   WARNING:
   到目前为止，您可以运行 `git rebase --abort` 来停止该过程。Git 中止 rebase 并将分支回滚到运行 `git rebase` 之前的状态。在运行 `git rebase --continue` 后，您不能中止 rebase。

1. [强制推送](../../../topics/git/git_rebase.md#强制推送)对远端分支的更改。

## 合并提交策略

极狐GitLab 通过在源分支中创建合并提交来解决冲突，但不会将其合并到目标分支中。然后，您可以查看和测试合并提交。验证它不包含意外更改，并且不会破坏您的构建。

<!--
## Related topics

- [Introduction to Git rebase and force-push](../../../topics/git/git_rebase.md).
- [Git GUI apps](https://git-scm.com/downloads/guis) to help you visualize the
  differences between branches and resolve them.
-->

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
