---
stage: Create
group: Code Review
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: index, reference
---

# 合并请求 **(FREE)**

合并请求 (MR) 是源代码更改进入到分支中的方式。
当您打开合并请求时，您可以在合并之前对代码更改进行可视化和协作。
合并请求包括：

- 请求的描述。
- 代码更改和内联代码审查。
- 有关 CI/CD 流水线的信息。
- 讨论主题的评论部分。
- 提交列表。

首先，阅读[合并请求简介](getting_started.md)。

## 查看合并请求

您可以查看您的项目、群组或您自己的合并请求。

### 查看项目的合并请求

查看项目的所有合并请求：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **合并请求**。

或者，要使用[键盘快捷键](../../shortcuts.md)，请按 <kbd>g</kbd> + <kbd>m</kbd>。

### 查看群组中所有项目的合并请求

查看群组中所有项目的合并请求：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到您的群组。
1. 在左侧边栏上，选择 **合并请求**。

如果您的群组包含子组，此视图还会显示来自子组项目的合并请求。

## 查看分配给您的所有合并请求

查看分配给您的所有合并请求：

1. 在顶部栏上，将光标放在 **搜索** 框中。
1. 从下拉列表中，选择 **指派给我的合并请求**。

或者，要使用[键盘快捷键](../../shortcuts.md)，请按 <kbd>Shift</kbd> + <kbd>m</kbd>。

<!--
You can [search and filter](../../search/index.md#filter-issue-and-merge-request-lists),
the results, or select a merge request to begin a review.-->您可以搜索和过滤结果，或选择合并请求以开始审核。

## 向合并请求添加更改

如果您有权向合并请求添加更改，则可以通过多种方式将更改添加到现有合并请求，具体取决于更改的复杂性以及您是否需要访问开发环境：

- 在浏览器中使用 <kbd>.</kbd> [键盘快捷键](../../shortcuts.md)，在 Web IDE 中编辑更改<!--[在 Web IDE 中编辑更改](../web_ide/index.md)-->。使用这种基于浏览器的方法来编辑多个文件，或者如果您对 Git 命令不满意。您不能从 Web IDE 运行测试。
- 如果您需要一个功能齐全的环境来编辑文件，然后运行测试，<!--[在 Gitpod 编辑更改](../../../integration/gitpod.md#launch-gitpod-in-gitlab)-->在 Gitpod 编辑更改。Gitpod 支持运行 GitLab Development Kit (GDK)。要使用 Gitpod，您必须在您的用户帐户中启用 Gitpod<!--[在您的用户帐户中启用 Gitpod](../../../integration/gitpod.md#enable-gitpod-in-your-user-settings)-->。
- 如果您熟悉 Git 和命令行，[从命令行推送更改](../../../gitlab-basics/start-using-git.md)。

## 关闭合并请求

如果您决定永久停止处理合并请求，建议您关闭合并请求而不是[删除它](#删除合并请求)。合并请求的作者和指派人，项目中具有开发者、维护者或拥有者角色的用户可以关闭项目中的合并请求：

1. 转到您要关闭的合并请求。
1. 滚动到页面底部的评论框。
1. 在评论框之后，选择 **关闭合并请求**。

极狐GitLab 关闭合并请求，但保留合并请求、其注释和任何相关流水线的记录。

### 删除合并请求

建议您关闭而不是删除合并请求。

WARNING:
您无法撤消对合并请求的删除。

要删除合并请求：

1. 以项目所有者角色的用户身份登录极狐GitLab。只有具有此角色的用户才能删除项目中的合并请求。
1. 前往您要删除的合并请求，然后选择 **编辑**。
1. 滚动到页面底部，然后选择 **删除合并请求**。

## 合并请求工作流

对于在团队中工作的软件开发人员：

1. 您检出一个新分支，并通过合并请求提交您的更改。
1. 您从团队收集反馈。
1. 您使用代码质量报告<!--[代码质量报告](code_quality.md)-->处理实现优化代码。
1. 您使用 GitLab CI/CD 中的单元测试报告<!--[单元测试报告](../../../ci/unit_test_reports.md)-->验证您的更改。
1. 您避免使用许可证与您项目的许可证合规报告<!--[许可证合规报告](../../compliance/license_compliance/index.md)-->不兼容的依赖项。
1. 您向您的经理请求[核准](approvals/index.md)。
1. 您的经理：
   1. 使用他们的最终审核推送提交。
   1. [核准合并请求](approvals/index.md)。
   1. 设置为[流水线成功时合并](merge_when_pipeline_succeeds.md)。
1. 使用 GitLab CI 的手动作业<!--[手动作业](../../../ci/jobs/job_control.md#create-a-job-that-must-be-run-manually)-->将您的更改部署到生产环境中。
1. 您的实施已成功交付给您的客户。

对于为您公司的网站编写网页的 Web 开发人员：

1. 您检出一个新分支并通过合并请求提交一个新页面。
1. 您从审核者那里收集反馈。
1. 您可以使用 Review Apps<!--[Review Apps](../../../ci/review_apps/index.md)--> 预览您的更改。
1. 您要求您的网页设计师实现。
1. 您向您的经理请求[核准](approvals/index.md)。
1. 一旦获得核准，您的合并请求将被[压缩和合并](squash_and_merge.md)，并通过 GitLab Pages 部署到 staging。
1. 您的生产团队[拣选](cherry_pick_changes.md)合并提交到生产环境中。

<!--
## Related topics

- [Create a merge request](creating_merge_requests.md)
- [Review a merge request](reviews/index.md)
- [Authorization for merge requests](authorization_for_merge_requests.md)
- [Testing and reports](testing_and_reports_in_merge_requests.md)
- [GitLab keyboard shortcuts](../../shortcuts.md)
-->