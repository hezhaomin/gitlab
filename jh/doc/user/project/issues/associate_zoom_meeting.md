---
stage: Plan
group: Project Management
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 将 Zoom 会议与议题相关联 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/16609) in GitLab 12.4.-->

为了进行事件管理的同步通信，允许将 Zoom 会议与议题相关联。在启动 Zoom 呼叫后，您需要一种将电话会议与议题相关联的方法。 这是为了让您的团队成员无需请求链接即可快速加入。

## 将 Zoom 会议添加到议题

要将 Zoom 会议与议题相关联，您可以使用[快速操作](../quick_actions.md#议题合并请求和史诗)。

在议题中，使用 `/zoom` 快速操作和有效的 Zoom 链接发表评论：

```shell
/zoom https://zoom.us/j/123456789
```

如果 Zoom 会议 URL 有效，并且您至少具有报告者权限<!--[报告者权限](../../permissions.md)-->，系统警报会通知您添加成功。议题的描述会自动编辑以包含 Zoom 链接，并且会在议题标题的正下方显示一个按钮。

![Link Zoom Call in Issue](img/zoom-quickaction-button.png)

您只能将单个 Zoom 会议附加到议题。如果您尝试使用 `/zoom` 快速操作添加第二个 Zoom 会议，它将不起作用。您需要先[删除它](#从议题中删除现有的-zoom-会议)。

## 从议题中删除现有的 Zoom 会议

与添加 Zoom 会议类似，您可以通过快速操作将其删除：

```shell
/remove_zoom
```

如果您至少有报告者权限<!--[报告者权限](../../permissions.md)-->，系统警报会通知您会议 URL 已成功删除。
