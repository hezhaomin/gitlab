---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 报告滥用 **(FREE)**

您可以向管理员报告其他用户的滥用行为。

一名管理员可以选择<!--[可以选择](admin_area/review_abuse_reports.md)-->：

- 删除用户，这会将他们从实例中删除。
- 阻止用户，拒绝他们访问实例。
- 或删除报告，保留用户对实例的访问权限。

您可以通过以下方式报告：

- [个人资料](#通过用户的个人资料报告滥用行为)
- [评论](#通过用户评论举报滥用行为)
- [议题](#报告议题中的滥用行为)
- [合并请求](#报告合并请求中的滥用行为)

## 通过用户的个人资料报告滥用行为

要从用户的个人资料页面报告滥用行为：

1. 点击用户个人资料右上角的感叹号举报滥用按钮。
1. 完成滥用报告。
1. 单击 **发送报告** 按钮。

## 通过用户评论举报滥用行为

举报用户评论中的滥用行为：

1. 单击垂直省略号 (⋮) 更多操作按钮打开下拉菜单。
1. 选择 **向管理员报告滥用行为**。
1. 完成滥用报告。
1. 单击 **发送报告** 按钮。

NOTE:
被举报用户评论的 URL 预填在滥用举报的 **消息** 字段中。

## 报告议题中的滥用行为

1. 在议题的右上角，选择垂直省略号（**{ellipsis_v}**）。
1. 选择 **报告滥用**。
1. 提交滥用报告。
1. 选择 **发送报告**。

## 报告合并请求中的滥用行为

1. 在合并请求中的右上角，您可以：
    - 选择 **报告滥用**。如果您无权关闭合并请求，则会显示此选项。
    - 在 **标记为草稿** 旁边，选择向下箭头 (**{chevron-down}**)，然后选择 **报告滥用**。
      如果您有权关闭合并请求，则会显示此选项。
1. 提交滥用报告。
1. 选择 **发送报告**。

## 管理滥用报告

管理员能够查看和解决滥用报告。
<!--For more information, see [abuse reports administration documentation](admin_area/review_abuse_reports.md).-->
