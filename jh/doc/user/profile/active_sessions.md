---
stage: none
group: unassigned
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: howto
---

# 活动会话

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/17867) in GitLab 10.8.
-->

极狐GitLab 列出了所有已登录到您帐户的设备。您可以查看会话，并撤销任何您不认识的会话。

## 列出所有活动会话

列出所有有效会话：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **活动会话**。

![Active sessions list](img/active_sessions_list.png)

## 活动会话限制

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/31611) in GitLab 12.6.
-->

极狐GitLab 允许用户同时拥有多达 100 个活动会话。如果活动会话数超过 100，则删除最早的会话。

## 撤销会话

要撤销活动会话：

1. 在顶部栏的右上角，选择您的头像。
1. 选择 **编辑个人资料**。
1. 在左侧边栏上，选择 **活动会话**。
1. 选择会话旁边的 **撤销**。当前会话无法撤销，因为这会使您退出极狐 GitLab。

NOTE:
当任何会话被撤销时，所有设备的所有 **记住我** 令牌都会被撤销。有关 **记住我** 功能的更多信息，请参阅 [“为什么我一直被注销？”](index.md#为什么我总是被注销)。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
