---
stage: Manage
group: Access
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
type: reference, howto, concepts
---

# 子组 **(FREE)**

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/2772) in GitLab 9.0.
-->

支持多达 20 个级别的子组，也称为嵌套组或分层组。

通过使用子组，您可以执行以下操作：

- **独立的内部/外部组织。** 因为每个群组都可以有自己的可见性级别（公共、内部或私有<!--[公共、内部或私有](../../../development/permissions.md#general-permissions)-->），您可以在同一保护伞下托管用于不同目的的组。
- **组织大型项目。**对于大型项目，子组可以更轻松地分离源代码部分的权限。
- **让管理人员和控制可见性更容易。** 根据他们的群组[成员资格](#成员资格)，为人们提供不同的权限<!--[权限](../../permissions.md#group-members-permissions)-->。

<!--
For more information on allowed permissions in groups and projects, see
[visibility levels](../../../development/permissions.md#general-permissions).
-->

## 概览

一个群组内可以有多个子组，同时一个组只能有一个直接的父组。它类似于目录或嵌套项目列表：

- Group 1
  - Group 1.1
  - Group 1.2
    - Group 1.2.1
    - Group 1.2.2
      - Group 1.2.2.1

在现实世界的示例中，想象维护一个 GNU/Linux 发行版，其中第一组是发行版的名称，随后的群组按如下方式拆分：

- Organization Group - GNU/Linux distro
  - Category Subgroup - Packages
    - (project) Package01
    - (project) Package02
  - Category Subgroup - Software
    - (project) Core
    - (project) CLI
    - (project) Android app
    - (project) iOS app
  - Category Subgroup - Infra tools
    - (project) Ansible playbooks

<!--
另一个示例是将极狐GitLab 想象成一个公司按如下方式拆分：

- Organization Group - GitLab
  - Category Subgroup - Marketing
    - (project) Design
    - (project) General
  - Category Subgroup - Software
    - (project) GitLab CE
    - (project) GitLab EE
    - (project) Omnibus GitLab
    - (project) GitLab Runner
    - (project) GitLab Pages daemon
  - Category Subgroup - Infra tools
    - (project) Chef cookbooks
  - Category Subgroup - Executive team
-->

在子组之间执行诸如转换或导入项目之类的操作时，其行为与在“群组/项目”级别执行这些操作时的行为相同。

## 创建一个子组

如果用户被明确添加为所有者（或维护者，如果启用该设置）到直接父组，则用户可以创建子组，即使管理员在其设置中禁用了群组创建。

要创建一个子组：

1. 在顶部栏上，选择 **菜单 > 群组** 并找到父组。
1. 在右上角，选择 **新建子组**。
1. 选择 **创建群组**。
1. 填写字段。查看不能用作组名的[保留名称](../../reserved_names.md)列表。
1. 选择 **创建群组**。

要创建子组，您必须是群组的所有者或维护者，具体取决于群组的设置。

默认情况下，在以下版本创建的组：

- 对于 12.2 或更高版本，所有者和维护者可以创建子组。
- 对于 12.1 或更早版本，所有者可以创建子组。

您可以更改以下设置：

- 作为群组所有者：
   1. 选择群组。
   1. 在左侧边栏上，选择 **设置 > 通用**。
   1. 展开 **权限, LFS, 2FA** 部分。
- 作为管理员：
   1. 在顶部栏上，选择 **菜单 > 管理员**。
   1. 在左侧边栏上，选择 **概览 > 群组**。
   1. 选择群组，然后选择 **编辑**。

<!--
For more information, view the [permissions table](../../permissions.md#group-members-permissions).
-->

## 成员资格

将成员添加到群组时，该成员也会添加到所有子组。权限级别从组的父级继承。如果您在其父组之一中具有成员资格，则允许访问子组。

子组中流水线的作业可以使用注册到父组的 runner<!--[runner](../../../ci/runners/index.md)-->。这意味着为父组配置的机密可用于子组作业。

此外，属于子组的项目的维护者可以查看注册到父组的 runner 的详细信息。

成员的组权限只能由所有者更改，并且只能在添加成员的组的 **成员** 页面上更改。

您可以通过查看组的 **成员** 页面来判断成员是否继承了父组的权限。

![Group members page](img/group_members_v14_4.png)

从上图，我们可以推断出以下几点： 

- 有 5 名成员可以访问群组 `4`。
- 用户 0 是报告者，并从群组 `1` 继承了他们的权限，该群组位于群组 `4` 的层次结构之上。
- 用户 1 是开发者，并从群组 `1/2` 继承了他们的权限，该组位于群组 `4` 的层次结构之上。
- 用户 2 是开发者，并且从群组 `1/2/3` 继承了他们的权限，该组高于群组 `4` 的层次结构。
- 对于用户 3，**来源** 列表示 **直接成员**，因此他们属于我们正在查看的群组 `4`。
- 管理员是 **所有** 子组的所有者和成员，因此，与用户 3 一样，**来源** 列指示 **直接成员**。

成员可以[根据继承或直接成员资格过滤](../index.md#过滤群组)。

### 覆盖祖先组成员资格

NOTE:
您必须是群组的所有者才能向群组添加成员。

NOTE:
子组中用户的权限不能低于其任何祖先组中的权限。因此，您不能减少子组中用户相对于其祖先组的权限。

要覆盖用户在祖先组（他们被添加到的第一个组）的成员资格，请再次将该用户添加到具有更高权限集的新子组。

例如，如果首先将用户 1 添加到具有开发人员权限的群组 `1/2` 中，那么他们会在群组 `1/2` 的每个其他子组中继承这些权限。要为群组 `1/2/3/4` 赋予他们维护者角色<!--[维护者角色](../../permissions.md)-->，您需要将他们作为维护者再次添加到该组中。从该组中删除它们，权限会回退到祖先组的权限。

## 提及子组

在议题、提交和合并请求中提及组（`@group`）将通知该组的所有成员。现在有了子组，如果您想拆分组的结构，则有更细粒度的支持。提及和以前一样工作，您可以选择要通知的人群。

![Mentioning subgroups](img/mention_subgroups.png)

## 限制

以下是您不能对子组执行的操作的列表：

- GitLab Pages<!--[GitLab Pages](../../project/pages/index.md)--> 支持在子组下托管的项目，但不支持子组网站。这意味着只有最高级别的组支持群组网站<!--[群组网站](../../project/pages/getting_started_part_one.md#gitlab-pages-default-domain-names)-->，尽管您可以在子组下拥有项目网站。
- 无法与作为项目所在组的祖先的群组共享项目，取决于层次结构。例如，`group/subgroup01/project`**不能**与 `group` 共享，但可以与`group/subgroup02` 或 `group/subgroup01/subgroup03` 共享。

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
