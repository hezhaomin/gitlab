---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
comments: false
description: Read through the GitLab installation methods.
type: index
---

# 安装 **(FREE SELF)**

<!--GitLab can be installed in most GNU/Linux distributions, and with several
cloud providers. To get the best experience from GitLab, you must balance
performance, reliability, ease of administration (backups, upgrades, and
troubleshooting), and the cost of hosting.-->

极狐GitLab 可以安装于大多数 GNU/Linux 发行版，以及一些云服务提供商。为了获得最佳体验，您必须在性能、可靠性、易管理性（备份、升级和故障排查）和托管成本之间取得平衡。

## 需求

<!--Before you install GitLab, be sure to review the [system requirements](requirements.md).
The system requirements include details about the minimum hardware, software,
database, and additional requirements to support GitLab.-->

在您安装极狐GitLab 之前，确保已阅读[系统需求](requirements.md)。系统需求包括关于支持极狐GitLab 的最低硬件配置、软件需求、数据库和其它需求的详细信息。

## 选择安装方式

<!--Depending on your platform, select from the following available methods to
install GitLab:-->

在以下可用安装方式中，根据您的平台选择安装极狐GitLab 的方式。

| 安装方式                                            | 描述 | 何时选择 |
|----------------------------------------------------------------|-------------|----------------|
| [Linux 安装包](https://docs.gitlab.cn/omnibus/installation/) | 官方的 deb/rpm 安装包（也被称作 Omnibus GitLab）包含极狐GitLab 和依赖的组件，包括 PostgreSQL、Redis 和 Sidekiq。 | 入门的推荐方式。Linux 安装包是成熟的和可扩展的。<!--，现在用于 GitLab.cn。如果您需要额外的灵活性和弹性，我们建议您按照[参考架构文档](../administration/reference_architectures/index.md)中的说明部署极狐 GitLab。--> |
| [Helm charts](https://docs.gitlab.cn/charts/)                 | 用于在 Kubernetes 上安装极狐GitLab 及其所有组件的云原生 Helm chart。 | 在 Kubernetes 上安装极狐GitLab 时，您需要注意一些权衡：<br/>- 管理和故障排查需要 Kubernetes 知识。<br/>- 对于较小的安装，它可能会更昂贵。默认安装需要比单节点 Linux 包部署更多的资源，因为大多数服务都是以冗余方式部署的。<br/>- 有一些功能需要注意的限制。<!--[需要注意的限制](https://docs.gitlab.cn/charts/#limitations).--><br/><br/> 如果您的基础设施是基于 Kubernetes 构建的并且您熟悉它的工作原理，请使用此方法。管理方法、可观察性和一些概念与传统部署不同。 |
| [Docker](https://docs.gitlab.cn/jh/install/docker.html)              | Docker 容器化的极狐GitLab 软件包。 | 如果您熟悉 Docker，请使用此方法。 |

<!--
## Install GitLab on cloud providers

Regardless of the installation method, you can install GitLab on several cloud
providers, assuming the cloud provider supports it. Here are several possible installation
methods, the majority which use the Linux packages:

| Cloud provider                                                | Description |
|---------------------------------------------------------------|-------------|
| [AWS (HA)](aws/index.md)                                      | Install GitLab on AWS using the community AMIs provided by GitLab. |
| [Google Cloud Platform (GCP)](google_cloud_platform/index.md) | Install GitLab on a VM in GCP. |
| [Azure](azure/index.md)                                       | Install GitLab from Azure Marketplace. |
| [DigitalOcean](https://about.gitlab.com/blog/2016/04/27/getting-started-with-gitlab-and-digitalocean/) | Install GitLab on DigitalOcean. You can also [test GitLab on DigitalOcean using Docker Machine](digitaloceandocker.md). |
-->

## 下一步

<!--
After you complete the steps for installing GitLab, you can
[configure your instance](next_steps.md).
-->

在您完成安装极狐GitLab 的步骤后，您可以[设置您的实例](next_steps.md)。
