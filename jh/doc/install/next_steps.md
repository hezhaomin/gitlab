---
stage: Enablement
group: Distribution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 安装后续操作 **(FREE SELF)**

<!--
Here are a few resources you might want to check out after completing the
installation.
-->

在完成安装之后，您可能想要查看以下资源。

## 电子邮件和通知

- [SMTP](https://docs.gitlab.cn/omnibus/settings/smtp.html): 配置 SMTP，以获得适当的电子邮件通知支持。

## CI/CD

<!--
- [Set up runners](https://docs.gitlab.com/runner/): Set up one or more GitLab
  Runners, the agents that are responsible for all of the GitLab CI/CD features.
- [GitLab Pages](../administration/pages/index.md): Configure GitLab Pages to
  allow hosting of static sites.
- [GitLab Registry](../administration/packages/container_registry.md): With the
  GitLab Container Registry, every project can have its own space to store Docker
  images.
-->

<!--- [设置 runners](https://docs.gitlab.cn/runner/): 设置一个或多个 GitLab Runner，负责代理所有 GitLab CI/CD 功能。-->
- [GitLab Pages](../administration/pages/index.md): 配置 GitLab Pages 允许托管静态站点。
- [GitLab Registry](../administration/packages/container_registry.md)：使用容器制品仓库，每个项目都可以拥有自己的空间存储 Docker 镜像。

<!--
## 安全

- [Secure GitLab](../security/index.md#securing-your-gitlab-installation)：保护您的 GitLab 实例的推荐实践。

- Sign up for the GitLab [Security Newsletter](https://about.gitlab.com/company/preference-center/) to get notified for security updates upon release.
-->

## 用户认证

<!--
- [LDAP](../administration/auth/ldap/index.md): Configure LDAP to be used as
  an authentication mechanism for GitLab.
- [SAML and OAuth](../integration/omniauth.md): Authenticate via online services like Okta, Google, Azure AD, and more.
-->

- [LDAP](../administration/auth/ldap/index.md)：配置 LDAP，作为极狐GitLab 的用户认证机制。
<!--- [SAML and OAuth](../integration/omniauth.md)：通过例如 Okta、Google、Azure AD 等在线服务进行用户认证。-->

<!--
## Backup and upgrade

- [Back up and restore GitLab](../raketasks/backup_restore.md): Learn the different
  ways you can back up or restore GitLab.
- [Upgrade GitLab](../update/index.md): Every 22nd of the month, a new feature-rich GitLab version
  is released. Learn how to upgrade to it, or to an interim release that contains a security fix.
- [Release and maintenance policy](../policy/maintenance.md): Learn about GitLab
  policies governing version naming, as well as release pace for major, minor, patch,
  and security releases.
-->

## 许可证

<!--
- [Upload a license](../user/admin_area/license.md) or [start a free trial](https://about.gitlab.com/free-trial/):
  Activate all GitLab Enterprise Edition functionality with a license.
- [Pricing](https://about.gitlab.com/pricing/): Pricing for the different tiers.
-->

- [上传许可证](../user/admin_area/license.md) or [开始免费试用](https://about.gitlab.cn/free-trial/)：使用许可证激活极狐GitLab 的所有功能。
- [定价](https://about.gitlab.cn/pricing/)：不同级别版本的定价。

<!--
## 跨库代码搜索

- [Advanced Search](../integration/elasticsearch.md): Leverage Elasticsearch for
  faster, more advanced code search across your entire GitLab instance.
-->

<!--
## 扩展和复制

- [Scaling GitLab](../administration/reference_architectures/index.md):
  GitLab supports several different types of clustering.
- [Geo replication](../administration/geo/index.md):
  Geo is the solution for widely distributed development teams.

- [扩展极狐GitLab](../administration/reference_architectures/index.md)：极狐GitLab 支持几种不同的集群规模。
- [Geo 复制](../administration/geo/index.md)：广泛分散的开发团队可以使用 Geo 作为解决方案。
-->