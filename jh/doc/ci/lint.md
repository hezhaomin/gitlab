---
stage: Verify
group: Pipeline Authoring
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
---

# 使用 CI Lint 工具验证 `.gitlab-ci.yml` 语法 **(FREE)**

如果要在提交更改之前测试 GitLab CI/CD 配置的有效性，可以使用 CI Lint 工具。默认情况下，此工具会检查语法和逻辑错误，并且可以模拟流水线创建以尝试查找更复杂的问题。

要访问 CI Lint 工具，请导航到项目中的 **CI/CD > 流水线** 或 **CI/CD > 作业**，然后单击 **CI lint**。

如果您使用 VS Code，您还可以使用 GitLab Workflow VS Code 扩展<!--[GitLab Workflow VS Code 扩展](../user/project/repository/vscode.md)-->验证您的 CI/CD 配置。

## 验证基本逻辑和语法

默认情况下，CI lint 检查 CI YAML 配置的语法，并运行一些基本的逻辑验证。使用 [`includes` 关键字](yaml/index.md#include) 添加的配置也经过验证。

要使用 CI lint，请将完整的 CI 配置（例如`.gitlab-ci.yml`）粘贴到文本框中，然后单击 **验证**：

![CI Lint](img/ci_lint.png)

## 流水线模拟

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/229794) in GitLab 13.3.
-->

并非所有管道配置问题都可以通过[基本 CI lint 验证](#验证基本逻辑和语法)找到。
您可以模拟流水线的创建以进行更深入的验证，从而发现更复杂的问题。

要通过运行流水线模拟来验证配置：

1. 将要验证的 GitLab CI 配置粘贴到文本框中。
1. 选中 **为默认分支模拟流水线创建** 复选框。
1. 选择 **验证**。

![Dry run](img/ci_lint_dry_run.png)

### 流水线模拟限制

模拟作为针对默认分支的 `git push` 事件运行。您必须具有权限才能在此分支上创建流水线，以通过模拟进行验证。
