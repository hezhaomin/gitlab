---
stage: Verify
group: Pipeline Execution
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://about.gitlab.com/handbook/engineering/ux/technical-writing/#assignments
disqus_identifier: 'https://docs.gitlab.com/ee/user/project/pipelines/settings.html'
type: reference, howto
---

# 自定义流水线配置 **(FREE)**

您可以自定义流水线为您的项目运行的方式。

<!--
<i class="fa fa-youtube-play youtube" aria-hidden="true"></i>
For an overview of pipelines, watch the video [GitLab CI Pipeline, Artifacts, and Environments](https://www.youtube.com/watch?v=PCKDICEe10s).
Watch also [GitLab CI pipeline tutorial for beginners](https://www.youtube.com/watch?v=Jav4vbUrqII).
-->

## 更改哪些用户可以查看您的流水线

对于公共和内部项目，您可以更改谁可以看到您的：

- 流水线
- 作业输出日志
- 作业产物
- 流水线安全仪表板<!--[流水线安全仪表板](../../user/application_security/security_dashboard/index.md#pipeline-security)-->

但是：

- 来宾用户和非项目成员永远不会看到作业输出日志和产物。

要更改流水线和相关功能的可见性：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 选中或清除 **公开流水线** 复选框。
   当它被选中时，流水线和相关功能是可见的：

   - 对于 **公开** 项目，所有人。
   - 对于 **内部** 项目，除外部用户之外的所有登录用户。
   - 对于 **私有** 项目，所有项目成员（访客或更高级别）。

   清除时：

   - 对于 **公开** 项目，流水线对所有人可见。相关功能仅对项目成员（报告者或更高级别）可见。
   - 对于 **内部** 项目，除外部用户外，所有登录用户都可以看到流水线。
     相关功能仅对项目成员（报告者或更高级别）可见。
   - 对于 **私有** 项目，流水线和相关功能仅对项目成员（报告者或更高级别）可见。

## 自动取消冗余流水线

您可以将挂起或正在运行的流水线设置为在同一分支上运行新流水线时自动取消。您可以在项目设置中启用此功能：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 选中 **自动取消多余的流水线** 复选框。
1. 选择 **保存修改**。

使用 [`interruptible`](../yaml/index.md#interruptible) 关键字来指示是否可以在完成之前取消正在运行的作业。

## 跳过过期的部署作业

> 引入于 12.9 版本

您的项目可能有多个并发部署作业，这些作业计划在同一时间范围内运行。

这可能导致较旧的部署作业在较新的部署作业之后运行的情况，这可能不是您想要的。

为避免这种情况：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 选中 **跳过已过时的部署作业** 复选框。
1. 选择 **保存修改**。

新部署开始时，旧部署作业将被跳过。

<!--
For more information, see [Deployment safety](../environments/deployment_safety.md).
-->

## 重试过时的作业

> 引入于 13.6 版本

部署作业可能会因为运行了较新的作业而失败。如果您重试失败的部署作业，则环境可能会被较旧的源代码覆盖。如果您单击 **重试**，则会出现一个窗口警告您并要求确认。

<!--
For more information, see [Deployment safety](../environments/deployment_safety.md).
-->

## 指定自定义 CI/CD 配置文件

> 支持外部`.gitlab-ci.yml` 位置引入于 12.6 版本

系统期望在项目的根目录中找到 CI/CD 配置文件（`.gitlab-ci.yml`）。但是，您可以指定备用文件名路径，包括项目外的位置。

自定义路径：

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **CI/CD 配置文件** 字段中，输入文件名。如果文件：
    - 不在根目录中，包含路径。
    - 在不同的项目中，包括群组和项目名称。
    - 在外部站点上，输入完整的 URL。
1. 选择 **保存修改**。

### 自定义 CI/CD 配置文件示例

如果 CI/CD 配置文件不在根目录中，则路径必须是相对于它的。
例如：

- `my/path/.gitlab-ci.yml`
- `my/path/.my-custom-file.yml`

如果 CI/CD 配置文件位于外部站点上，则 URL 必须以 `.yml` 结尾：

- `http://example.com/generate/ci/config.yml`

如果 CI/CD 配置文件在不同的项目中：

- 文件必须存在于其默认分支上，或者将分支指定为 refname。
- 路径必须相对于其他项目中的根目录。
- 路径必须在末尾包含群组和项目名称。

例如：

- `.gitlab-ci.yml@mygroup/another-project`
- `my/path/.my-custom-file.yml@mygroup/another-project`
- `my/path/.my-custom-file.yml@mygroup/another-project:refname`

如果配置文件在单独的项目中，可以设置更细粒度的权限。例如：

- 创建一个公共项目来托管配置文件。
- 仅向允许编辑文件的用户授予对项目的写入权限。

这样其他用户和项目就可以访问该配置文件而不能对其进行编辑。

## 选择默认的 Git 策略

您可以选择在作业运行时如何从极狐GitLab 获取您的仓库。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **Git 策略** 下，选择一个选项：
    - `git clone` 速度较慢，因为它为每个作业从头开始克隆仓库。但是，本地工作副本始终是 origin 的。
    - `git fetch` 更快，因为它重新使用本地工作副本（如果它不存在，则回退到克隆）。这是推荐方法，特别是对于大型仓库<!--[大型仓库](../large_repositories/index.md#git-strategy)-->。

配置的 Git 策略可以被 `.gitlab-ci.yml` 文件中的 `GIT_STRATEGY` 变量<!--[`GIT_STRATEGY` 变量](../runners/configure_runners.md#git-strategy)-->覆盖。

## 限制克隆期间获取的更改数量

<!--
> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/28919) in GitLab 12.0.
-->

您可以限制 GitLab CI/CD 在克隆仓库时获取的更改数量。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **Git 策略** 下，在 **Git 浅克隆** 下，输入一个值。
    最大值为`1000`。要禁用浅克隆并使 GitLab CI/CD 每次都获取所有分支和标签，请将值保留为空或设置为 `0`。

在 12.0 及更高版本中，新创建的项目自动具有默认的 `git depth` 值为 `50`。

这个值可以被 `.gitlab-ci.yml` 文件中的 <!--[`GIT_DEPTH` 变量](../large_repositories/index.md#shallow-cloning)-->`GIT_DEPTH` 变量覆盖。

## 设置作业可以运行的时间限制

您可以定义作业在超时之前可以运行多长时间。

1. 在顶部栏上，选择 **菜单 > 项目s** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **超时** 字段中，输入分钟数或人类可读的值，如 `2 hours`。

超过超时的作业被标记为失败。

您可以为单个 runner<!--[为单个 runner](../runners/configure_runners.md#set-maximum-job-timeout-for-a-runner)--> 覆盖此值。

## 将测试覆盖率结果添加到合并请求

如果您在代码中使用测试覆盖率，则可以使用正则表达式在作业日志中查找覆盖率结果。然后，您可以将这些结果包含在极狐GitLab 的合并请求中。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **测试覆盖率解析** 字段中，输入正则表达式。
    留空以禁用此功能。

您可以使用 <https://rubular.com> 来测试您的正则表达式。正则表达式返回在输出中找到的**最后的**匹配项。

如果流水线成功，则覆盖范围将显示在合并请求部件和作业表中。如果流水线中的多个作业具有覆盖率报告，则对它们进行平均。

![MR widget coverage](img/pipelines_test_coverage_mr_widget.png)

![Build status coverage](img/pipelines_test_coverage_build.png)

### 测试覆盖率示例

将此正则表达式用于常用的测试工具。

<!-- vale gitlab.Spelling = NO -->

- Simplecov (Ruby). Example: `\(\d+.\d+\%\) covered`.
- pytest-cov (Python). Example: `^TOTAL.+?(\d+\%)$`.
- Scoverage (Scala). Example: `Statement coverage[A-Za-z\.*]\s*:\s*([^%]+)`.
- `phpunit --coverage-text --colors=never` (PHP). Example: `^\s*Lines:\s*\d+.\d+\%`.
- gcovr (C/C++). Example: `^TOTAL.*\s+(\d+\%)$`.
- `tap --coverage-report=text-summary` (NodeJS). Example: `^Statements\s*:\s*([^%]+)`.
- `nyc npm test` (NodeJS). Example: `All files[^|]*\|[^|]*\s+([\d\.]+)`.
- excoveralls (Elixir). Example: `\[TOTAL\]\s+(\d+\.\d+)%`.
- `mix test --cover` (Elixir). Example: `\d+.\d+\%\s+\|\s+Total`.
- JaCoCo (Java/Kotlin). Example: `Total.*?([0-9]{1,3})%`.
- `go test -cover` (Go). Example: `coverage: \d+.\d+% of statements`.
- .NET (OpenCover). Example: `(Visited Points).*\((.*)\)`.
- .NET (`dotnet test` line coverage). Example: `Total\s*\|\s*(\d+(?:\.\d+)?)`.
- tarpaulin (Rust). Example: `^\d+.\d+% coverage`.

<!-- vale gitlab.Spelling = YES -->

### 查看代码覆盖率历史

<!--
> - [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/209121) the ability to download a `.csv` in GitLab 12.10.
> - Graph [introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/33743) in GitLab 13.1.
-->

要查看项目代码覆盖率随时间的演变，您可以查看图表或下载包含此数据的 CSV 文件。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **分析 > 仓库**。

每个作业的历史数据都列在图表上方的下拉列表中。

要查看数据的 CSV 文件，请选择 **下载原始数据 (`.csv`)**。

![Code coverage graph of a project over time](img/code_coverage_graph_v13_1.png)

代码覆盖率数据也在群组级别可用<!--[在群组级别可用](../../user/group/repositories_analytics/index.md)-->。

### 覆盖检查批准规则 **(PREMIUM)**

> - 引入于 14.0 版本。
> - 支持在项目设置中进行配置于 14.1 版本。

当合并请求会导致项目的测试覆盖率下降时，您可以实施合并请求批准，以要求选定用户或群组的批准。

请按照以下步骤启用 `Coverage-Check` MR 批准规则：

1. 为要包含在整体覆盖率值中的所有作业，设置 [`coverage`](../yaml/index.md#coverage) 正则表达式。
1. 转到您的项目并选择 **设置 > 通用**。
1. 展开 **合并请求批准**。
1. 选择 `Coverage-Check` 批准规则旁边的 **启用**。
1. 选择 **目标分支**。
1. 将 **需要核准** 设置为大于零。
1. 选择要提供批准的用户或群组。
1. 选择 **添加批准规则**。

![Coverage-Check approval rule](img/coverage_check_approval_rule_14_1.png)

### 从代码覆盖率中删除颜色代码

一些测试覆盖工具输出的 ANSI 颜色代码未被正则表达式正确解析。 这会导致覆盖解析失败。

某些覆盖工具不提供在输出中禁用颜色代码的选项。如果是这样，请通过一个小的单行脚本，将覆盖工具的输出通过流水线来去除颜色代码。

例如：

```shell
lein cloverage | perl -pe 's/\e\[?.*?[\@-~]//g'
```

## 流水线徽章

流水线徽章指示项目的流水线状态和测试覆盖率值。这些徽章由最新的成功流水线决定。

### 查看流水线状态和覆盖率报告徽章的代码

您可以查看徽章的确切链接。然后您可以在 HTML 或 Markdown 页面中嵌入徽章。

1. 在顶部栏上，选择 **菜单 > 项目** 并找到您的项目。
1. 在左侧边栏上，选择 **设置 > CI/CD**。
1. 展开 **流水线通用设置**。
1. 在 **流水线状态** 或 **覆盖率报告** 部分，查看图像的 URL。

![Pipelines badges](img/pipelines_settings_badges.png)

### 流水线状态徽章

根据流水线的状态，徽章可以具有以下值：

- `pending`
- `running`
- `passed`
- `failed`
- `skipped`
- `canceled`
- `unknown`

您可以使用以下链接访问流水线状态徽章图像：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/pipeline.svg
```

#### 仅显示非跳过状态

要使流水线状态徽章仅显示最后一个未跳过的状态，请使用 `?ignore_skipped=true` 查询参数：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/pipeline.svg?ignore_skipped=true
```

### 测试覆盖率报告徽章

您可以为每个作业日志匹配的[覆盖率报告](#将测试覆盖率结果添加到合并请求)定义正则表达式。这意味着流水线中的每个作业都可以定义测试覆盖率百分比值。

要访问测试覆盖率徽章，请使用以下链接：

```plaintext
https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg
```

要从特定作业获取覆盖率报告，请将 `job=coverage_job_name` 参数添加到 URL。例如，以下 Markdown 代码在您的 `README.md` 中嵌入了 `coverage` 作业的测试覆盖率报告徽章：

```markdown
![coverage](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage)
```

#### 测试覆盖率报告徽章颜色和限制

徽章的默认颜色和限制如下：

- 95 到 100% - 好 (`#4c1`)
- 90 到 95% - 可接受 (`#a3c51c`)
- 75 到 90% - 中等 (`#dfb317`)
- 0 到 75% - 低 (`#e05d44`)
- 没有覆盖 - 未知（`#9f9f9f`）

<!--
NOTE:
*Up to* means up to, but not including, the upper bound.
-->

您可以使用以下附加参数覆盖限制（引入于 14.4 版本）：

- `min_good` (默认 95，可以使用 3 到 100 之间的任何值)
- `min_acceptable` (默认 90，可以使用 2 和 min_good-1 之间的任何值)
- `min_medium` (默认 75，可以使用 1 到 min_acceptable-1 之间的任何值)

如果设置了无效边界，系统会自动将其调整为有效。例如，`min_good` 设置为`80`，并且 `min_acceptable` 设置为 `85`（太高），系统会自动将 `min_acceptable` 设置为 `79`（`min_good` - `1`）。

### 徽章样式

通过向 URL 添加 `style=style_name` 参数，可以不同的样式呈现流水线徽章。有两种款式可供选择：

- 平面（默认）：

  ```plaintext
  https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?style=flat
  ```

  ![Badge flat style](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage&style=flat)

- 平方<!--([Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/issues/30120) in GitLab 11.8)-->：

  ```plaintext
  https://gitlab.example.com/<namespace>/<project>/badges/<branch>/coverage.svg?style=flat-square
  ```

  ![Badge flat square style](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=coverage&style=flat-square)

### 自定义徽章文本

> 引入于 13.1 版本

可以自定义徽章的文本，区分在同一流水线中运行的多个覆盖作业。通过向 URL 添加 `key_text=custom_text` 和 `key_width=custom_key_width` 参数来自定义徽章文本和宽度：

```plaintext
https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=karma&key_text=Frontend+Coverage&key_width=130
```

![Badge with custom text and width](https://gitlab.com/gitlab-org/gitlab/badges/main/coverage.svg?job=karma&key_text=Frontend+Coverage&key_width=130)

<!-- ## Troubleshooting

Include any troubleshooting steps that you can foresee. If you know beforehand what issues
one might have when setting this up, or when something is changed, or on upgrading, it's
important to describe those, too. Think of things that may go wrong and include them here.
This is important to minimize requests for support, and to avoid doc comments with
questions that you know someone might ask.

Each scenario can be a third-level heading, e.g. `### Getting error message X`.
If you have none to add when creating a doc, leave this section in place
but commented out to help encourage others to add to it in the future. -->
