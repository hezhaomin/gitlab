# frozen_string_literal: true

scope(path: 'users/:phone',
      as: :user,
      controller: :users) do
  get :phone_exists
end
