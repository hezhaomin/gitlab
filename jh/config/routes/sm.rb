# frozen_string_literal: true

resources :sms, only: [:verification_code] do
  collection do
    post :verification_code
  end
end
