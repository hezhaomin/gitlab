# frozen_string_literal: true

module JH
  module WikiActions
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    private

    override :send_wiki_file_blob
    def send_wiki_file_blob(wiki, file_blob)
      return super unless ::ContentValidation::Setting.block_enabled?(wiki)

      commit = wiki.repository.commit(wiki.default_branch)
      content_blocked_state = ::ContentValidation::ContentBlockedState.find_by_container_commit_path(wiki, commit&.id, file_blob.path)

      return super unless content_blocked_state.present?

      render plain: s_("ContentValidation|According to the relevant laws and regulations, this content is not displayed.")
    end
  end
end
