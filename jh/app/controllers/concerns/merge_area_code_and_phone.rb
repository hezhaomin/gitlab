# frozen_string_literal: true

module MergeAreaCodeAndPhone
  private

  # e.g: "+86" + "15612341234" = "+8615612341234"
  def merge_area_code_and_phone
    unless params[:phone].start_with?("+")
      params[:phone] = params[:area_code] + params[:phone] if params[:area_code]
    end
  end

  def merge_area_code_and_phone_with_user
    unless params[:user][:phone].start_with?("+")
      params[:user][:phone] = params[:user][:area_code] + params[:user][:phone] if params[:user][:area_code]
    end
  end
end
