# frozen_string_literal: true

module JH
  module Projects::BlameController
    extend ActiveSupport::Concern

    prepended do
      before_action :set_last_commit, only: [:show]
    end

    private

    # rubocop:disable Gitlab/ModuleWithInstanceVariables
    def set_last_commit
      _id, ref, path = extract_ref_path
      @last_commit = ::Gitlab::Git::Commit.last_for_path(repository, ref, path, literal_pathspec: true)
    end
    # rubocop:enable Gitlab/ModuleWithInstanceVariables
  end
end
