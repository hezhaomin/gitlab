# frozen_string_literal: true

module JH
  module BillingPlansHelper
    extend ::Gitlab::Utils::Override

    override :number_to_plan_currency
    def number_to_plan_currency(value)
      number_to_currency(value, unit: '¥', strip_insignificant_zeros: true, format: "%u%n")
    end

    override :use_new_purchase_flow?
    def use_new_purchase_flow?(namespace)
      false
    end
  end
end
