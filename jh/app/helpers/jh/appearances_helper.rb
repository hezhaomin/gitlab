# frozen_string_literal: true

module JH
  module AppearancesHelper
    extend ::Gitlab::Utils::Override

    override :default_brand_title
    def default_brand_title
      'JiHu GitLab'
    end
  end
end
