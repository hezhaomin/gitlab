# frozen_string_literal: true

module JH
  # User JH mixin
  #
  # This module is intended to encapsulate JH-specific model logic
  # and be prepended in the  model

  module Project
    extend ActiveSupport::Concern
    extend ::Gitlab::Utils::Override

    prepended do
      include ContentValidateable
      validates :title, :description, content_validation: true, if: :should_validate_content?

      def should_validate_content?
        !self.private? && super
      end
    end

    override :after_import
    def after_import
      super

      if ::ContentValidation::Setting.check_enabled?(self)
        ::ContentValidation::ContainerService.new(container: self, user: self.creator).execute
        ::ContentValidation::ContainerService.new(container: wiki, user: self.creator).execute if wiki.exists?
      end
    end
  end
end
