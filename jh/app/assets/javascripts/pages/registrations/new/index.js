import '~/pages/registrations/new';
import PhoneValidator from 'jh/pages/sessions/new/phone_validator';
import VerificationCodeButton from 'jh/verification_code_button';

const verificationButton = new VerificationCodeButton();
new PhoneValidator({ verificationButton }); // eslint-disable-line no-new
